#!/bin/bash

echo "Example long task running from 1 to 10"

for a in $(seq 1 10)
do
	for f in $(seq 1 100)
	do
			echo "$f ..."
	done
	echo "waiting ... $a"
	sleep 1
done

echo "Example task done"

exit 0
